package com.smedialink.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class DataSource {

    public static int NUMBER_OF_ROWS = 100;

    private SQLiteDatabase mSQLiteDatabase;
    private DbHelper mSQLiteHelper;

    private String[] mAllColumns = {DbHelper.COLUMN_ID,
            DbHelper.COLUMN_VALUE};

    public DataSource(Context context) {
        mSQLiteHelper = new DbHelper(context);
    }

    public void open() throws SQLiteException {
        mSQLiteDatabase = mSQLiteHelper.getWritableDatabase();
    }

    public void close() {
        mSQLiteHelper.close();
    }

    public void saveOrUpdateRow(Integer row, Float value) {
        ContentValues values = new ContentValues();
        values.put(DbHelper.COLUMN_VALUE, value);

        int res = mSQLiteDatabase.update(DbHelper.TABLE_SETTINGS, values, DbHelper.COLUMN_ID + " = " + row, null);

        if (res == 0) {
            values.put(DbHelper.COLUMN_ID, row);
            mSQLiteDatabase.insert(DbHelper.TABLE_SETTINGS, null, values);
        }
    }

    public Cursor getCursorForUserRows() {

        return mSQLiteDatabase.query(DbHelper.TABLE_SETTINGS,
                mAllColumns, null, null, null, null, null);
    }

    // should work not only for 100 rows )))
    // and do not need to save all 100 or N rows at onCreate(SQLiteDatabase db)
    // Database holds only values entered by user
    // query like this
    /*
        select a.id, s.value from demo.settings s
        right join
        (
            select 1 as id
            union
            select 2 as id
            union
            select 3 as id
            union
            ...
            union
            select N as id

        ) a
        on a.id = s._id
     */
    public Cursor getCursorForAllRows() {
        StringBuilder query = new StringBuilder("select a.id _id, s.value from  ( select 1 as id");
        for (int i = 2; i <= NUMBER_OF_ROWS; i++) {
            query.append(" union select ").append(i).append(" as id");
        }
        query.append(") a " +
                " left outer join settings s on a.id = s._id");

        return mSQLiteDatabase.rawQuery(query.toString(), null);
    }
}
