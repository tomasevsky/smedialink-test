package com.smedialink;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by tomashevsky on 27.03.2015.
 */
public class RowDetailActivity extends Activity {

    public static String EXTRA_MESSAGE_ROW = "com.smedialink.RowDetailActivity.SELECTED_ROW";
    public static String EXTRA_MESSAGE_VALUE = "com.smedialink.RowDetailActivity.SELECTED_VALUE";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_row_detail);
        Bundle extras = getIntent().getExtras();
        int rowNumber = extras.getInt(EXTRA_MESSAGE_ROW);
        Float rowValue = extras.getFloat(EXTRA_MESSAGE_VALUE);


        TextView rowNumberTextView = (TextView) this.findViewById(R.id.rowNumber);
        AdvancedTextView gradientButtonTextView = (AdvancedTextView) this.findViewById(R.id.gradientButton);
        // Populate fields with extracted properties
        rowNumberTextView.setText(String.valueOf(rowNumber));
        gradientButtonTextView.setValue(rowValue == null ? 0 : rowValue);
    }

}
