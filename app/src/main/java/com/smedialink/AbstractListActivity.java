package com.smedialink;

import android.app.ListActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.CursorAdapter;

import com.smedialink.db.DataSource;

/**
 * Created by tomashevsky on 27.03.2015.
 */
public abstract class AbstractListActivity extends ListActivity {
    protected DataSource mDataSource;
    protected CursorAdapter mAdapter;
    protected boolean showArrow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDataSource = new DataSource(this);
        mDataSource.open();

        mAdapter = new CustomCursorAdapter(this, this.createCursor(), this.showArrow);
        setListAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        mDataSource.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mDataSource.close();
        super.onPause();
    }

    abstract Cursor createCursor();
}
