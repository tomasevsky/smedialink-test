package com.smedialink;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.smedialink.db.DataSource;


public class SettingsActivity extends AbstractListActivity {

    private EditText rowEditText;
    private EditText percentEditText;
    private Button mAddButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

//        getActionBar().setDisplayHomeAsUpEnabled(true);


        rowEditText = (EditText) findViewById(R.id.row);
        percentEditText = (EditText) findViewById(R.id.percent);

        mAddButton = (Button) findViewById(R.id.add);

        mAddButton.setClickable(true);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputSetting();
            }
        });

    }

    @Override
    protected Cursor createCursor() {
        return mDataSource.getCursorForUserRows();
    }

    private void inputSetting() {

        String rowString = rowEditText.getText().toString();
        String valueString = percentEditText.getText().toString();

        if (rowString.isEmpty() || valueString.isEmpty()) {
            Toast.makeText(this, R.string.message_empty_data, Toast.LENGTH_SHORT).show();
            return;
        } else {
            Integer row;
            try {
                row = Integer.valueOf(rowString);
                if (row > DataSource.NUMBER_OF_ROWS || row < 0) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                Toast.makeText(this, R.string.message_wrong_row, Toast.LENGTH_SHORT).show();
                return;
            }
            Float value;
            try {
                value = Float.valueOf(valueString);
                if (value > 1 || value < 0) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                Toast.makeText(this, R.string.message_wrong_value, Toast.LENGTH_SHORT).show();
                return;
            }

            mDataSource.saveOrUpdateRow(row, value);
//            mAdapter.add(user);
            mAdapter.changeCursor(mDataSource.getCursorForUserRows());
            mAdapter.notifyDataSetChanged();
        }
    }
}