package com.smedialink;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

public class MainActivity extends AbstractListActivity {

    public MainActivity() {
        this.showArrow = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected Cursor createCursor() {
        return mDataSource.getCursorForAllRows();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onListItemClick(ListView l, View view, int position, long id) {
        Intent intent = new Intent(this, RowDetailActivity.class);
        AdvancedTextView editText = (AdvancedTextView) view.findViewById(R.id.gradientButton);
        // row number position in list view starts from 0;
        intent.putExtra(RowDetailActivity.EXTRA_MESSAGE_ROW, position + 1);
        intent.putExtra(RowDetailActivity.EXTRA_MESSAGE_VALUE, editText.getValue());
        startActivity(intent);
    }
}
