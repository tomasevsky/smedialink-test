package com.smedialink;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smedialink.db.DbHelper;

public class CustomCursorAdapter extends CursorAdapter {

    Activity context;
    private boolean showArrow = false;

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };

    public CustomCursorAdapter(Activity context, Cursor cursor, boolean showArrow) {
        super(context, cursor, true);
        this.context = context;
        this.showArrow = showArrow;
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = this.context.getLayoutInflater().inflate(R.layout.layout_row_item, parent, false);
        view.findViewById(R.id.gradientButton).setOnClickListener(mOnClickListener);
        return view;
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Extract properties from cursor
        Integer rowNumber = cursor.getInt(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_ID));
        Float rowValue = cursor.getFloat(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_VALUE));
        if (showArrow) {
            view.<ImageView>findViewById(R.id.arrow).setVisibility(View.VISIBLE);
        }

        // TODO: reusable code how to handle
        // Find fields to populate in inflated template
        TextView rowNumberTextView = (TextView) view.findViewById(R.id.rowNumber);
        AdvancedTextView gradientButtonTextView = (AdvancedTextView) view.findViewById(R.id.gradientButton);
        // Populate fields with extracted properties
        rowNumberTextView.setText(String.valueOf(rowNumber));
        gradientButtonTextView.setValue(rowValue == null ? 0 : rowValue);
    }
}